#!/usr/bin/env python

#
# This module connects to a Redis server and pings it to verify the connection.
# Requirements: pip install redis
#

import argparse

import redis


# Parse commandline aruments. Defaults are host=localhost, port=6379, db=0.
def getArgs():
   parser = argparse.ArgumentParser(description="Connect to a Redis server.")

   parser.add_argument("-redis_host", default="localhost", dest="host")
   parser.add_argument("-redis_port", type=int, default=6379, dest="port")
   parser.add_argument("-redis_db", type=int, default=0, dest="db")

   return parser.parse_args()


args = getArgs()

# Create a connection. Automatically decode all responses from bytes to strings.
rds = redis.Redis(host=args.host, port=args.port, db=args.db, decode_responses=True)

# Ping the server.
try:
   rds.ping()
except redis.connection.ConnectionError:
   print("Error: Redis connection unsuccessful.")
else:
   print("Redis connection successful.")
