#!/usr/bin/env bash

#
# This program converts the case of the filenames in a directory.
#

# Treat unset variables as errors.
set -u

# At least 2 args after the program name, the first of which must be -s or -S.
if [ $# -lt 2 ] || [ $1 != '-s' ] && [ $1 != '-S' ]; then
   echo "usage: $0 (-s|-S) target_dir"
   exit 1
fi

DIRNAME="$2"

# Exit if the second argument isn't a directory.
if [ ! -d "$DIRNAME" ]; then
   echo "Input Error: '$DIRNAME' must be a directory."
   exit 2
fi

# Loop through directory contents, one level deep.
for NAME in $DIRNAME/*; do # Globbing takes care of filenames containing spaces.
   if [ -f "$NAME" ]; then # Process only files.
      BASENAME=$(basename "$NAME") # The basename of /a/b.txt is b.txt

      if [ $1 == '-s' ]; then
         LOWERCASE_BASENAME=$(echo -n "$BASENAME" | tr [:upper:] [:lower:]) # Conversion happens here.
         mv "$DIRNAME/$BASENAME" "$DIRNAME/$LOWERCASE_BASENAME" 2> /dev/null
      else
         UPPERCASE_BASENAME=$(echo -n "$BASENAME" | tr [:lower:] [:upper:])
         mv "$DIRNAME/$BASENAME" "$DIRNAME/$UPPERCASE_BASENAME" 2> /dev/null
      fi
   fi
done
