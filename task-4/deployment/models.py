from django.db import models


# 'name' is the only reuqired field.
class Deployment(models.Model):
   name = models.CharField(max_length=255, unique=True)
   schedule_time = models.DateTimeField(null=True, blank=True)
   status = models.CharField(max_length=255, blank=True)
   issues_encountered = models.TextField(blank=True)
   description = models.TextField(blank=True)
   duration_of_employment = models.CharField(max_length=255, blank=True)

   def __str__(self):
      return self.name
