from django.test import TestCase

from .. import models


class TestModel(TestCase):
   def testDeploymentCreation(self):
      models.Deployment.objects.create(name="Test Deployment 1")
      models.Deployment.objects.create(name="Test Deployment 2")
      models.Deployment.objects.create(name="Test Deployment 3")

      self.assertEqual(models.Deployment.objects.count(), 3)
