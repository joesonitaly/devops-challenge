import json

from django.test import TestCase
from django.urls import reverse

from .. import models


class TestView(TestCase):
   def testDeploymentCreate(self):
      response = self.client.post(reverse("deployment-list"), {"name": "Test Deployment 1"})
      self.assertEqual(response.status_code, 201)

      response = self.client.post(reverse("deployment-list"), {"name": "Test Deployment 2"})
      self.assertEqual(response.status_code, 201)

      response = self.client.post(reverse("deployment-list"), {"name": "Test Deployment 3"})
      self.assertEqual(response.status_code, 201)

      self.assertEqual(models.Deployment.objects.count(), 3)

   def testDeploymentList(self):
      models.Deployment.objects.create(name="Test Deployment 1")
      models.Deployment.objects.create(name="Test Deployment 2")
      models.Deployment.objects.create(name="Test Deployment 3")

      self.assertEqual(models.Deployment.objects.count(), 3)

      response = self.client.get(reverse("deployment-list"))
      self.assertEqual(response.status_code, 200)

      responseContent = json.loads(response.content)

      self.assertIsInstance(responseContent, list)
      self.assertEqual(len(responseContent), 3)

   def testDeploymentDetail(self):
      models.Deployment.objects.create(name="Test Deployment 1")
      self.assertEqual(models.Deployment.objects.count(), 1)

      response = self.client.get(reverse("deployment-detail", kwargs={"name": "Test Deployment 0"}))
      self.assertEqual(response.status_code, 404)

      response = self.client.get(reverse("deployment-detail", kwargs={"name": "Test Deployment 1"}))
      self.assertEqual(response.status_code, 200)

      responseContent = json.loads(response.content)

      self.assertIsInstance(responseContent, dict)

   def testDeploymentPartialUpdate(self):
      models.Deployment.objects.create(name="Test Deployment 1")
      self.assertEqual(models.Deployment.objects.count(), 1)

      response = self.client.patch(reverse("deployment-detail", kwargs={"name": "Test Deployment 1"}), {"status": "PENDING"}, content_type="application/json")
      self.assertEqual(response.status_code, 200)

      self.assertEqual(models.Deployment.objects.count(), 1)

   def testDeploymentDelete(self):
      models.Deployment.objects.create(name="Test Deployment 1")
      self.assertEqual(models.Deployment.objects.count(), 1)

      response = self.client.delete(reverse("deployment-detail", kwargs={"name": "Test Deployment 1"}))
      self.assertEqual(response.status_code, 204)

      self.assertEqual(models.Deployment.objects.count(), 0)

   def testDeploymentSearch(self):
      models.Deployment.objects.create(name="Test Deployment 1")
      models.Deployment.objects.create(name="Test Deployment 2")
      models.Deployment.objects.create(name="Test Deployment 3")

      self.assertEqual(models.Deployment.objects.count(), 3)

      response = self.client.get(reverse("deployment-search"), {"name": "Test Deployment 0"})
      self.assertEqual(response.status_code, 200)

      responseContent = json.loads(response.content)

      self.assertIsInstance(responseContent, list)
      self.assertEqual(len(responseContent), 0)

      response = self.client.get(reverse("deployment-search"), {"name": "Test Deployment 2"})
      self.assertEqual(response.status_code, 200)

      responseContent = json.loads(response.content)

      self.assertIsInstance(responseContent, list)
      self.assertEqual(len(responseContent), 1)
