from django.urls import path
from django.views.generic import RedirectView

from rest_framework.routers import SimpleRouter

from . import views


# Route endpoints to corresponding views.
router = SimpleRouter()
router.register("deployments", views.DeploymentViewSet)

urlpatterns = router.urls + [path("", RedirectView.as_view(pattern_name="deployment-list", permanent=True))]
