from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from . import models, serializers


# This view uses 'name' as the looup field instead of 'pk'.
class DeploymentViewSet(viewsets.ModelViewSet):
   lookup_field = "name"
   queryset = models.Deployment.objects.all()
   serializer_class = serializers.DeploymentSerializer

   # '/deployment/search/' endpoint.
   @action(detail=False)
   def search(self, request):
      deployments = self.get_queryset().filter(**dict(request.query_params.items())) # Filter by GET parameters.
      serializer = self.get_serializer(deployments, many=True)
      return Response(serializer.data)
