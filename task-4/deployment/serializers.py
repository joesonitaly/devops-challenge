from rest_framework import serializers

from . import models


# Serializes and deserializes Deployment objects.
class DeploymentSerializer(serializers.ModelSerializer):
   class Meta:
      model = models.Deployment
      fields = "__all__"
