#DevOps Challenge

##Description
This repository contains the solution to the DevOps Challenge. Four tasks were assigned.

* Task 1 mentions five advantages of using Kubernetes. The solution to this task is in `task-1/Kubernetes.txt`.
* Task 2 is a Bash script that takes two arguments and converts the filenames in a target directory to uppercase or lowercase.
The solution to this task is in `task-2/check_file`. Execution takes the form `./check-file (-s|-S) target_dir`.
`-s` converts the filenames to lowercase. `-S` converts the filenames to uppercase.
Bash was used for this task because it requires less code than, say, a Python script.
* Task 3 is a Python script that connects to a running Redis server, `ping`ing it to verify a successful connection.
The solution to this task is in `task-3/test_redis.py`. Execution takes the form `./test_redis [-redis_host HOST] [-redis_port PORT] [-redisdb DB]`.
All arguments to `test_redis` are optional, in which case the default host ("localhost"), port (6379) and db (0) are used.
The `redis` python package is required.
* Task 4 is a Python/Django/DjangoRestFramework application that exposes the following API endpoints:

Name | Method | Endpoint
---- | ------ | ---
List | GET | /deployments/
Create | POST | /deployments/
Detail | GET | /deployments/{name}/
Update | PUT | /deployments/{name}/
Partial Update | PATCH | /deployments/{name}/
Delete | DELETE | /deployments/{name}/
Search | GET | /search?x=a&y=b

It includes comprehensive unit tests (run with `./manage.py tests`) and Docker/docker-compose configurations.

##Setup
After cloning this repository, change into cloned repository. Each solution can then be evaluated.

* Change into `task-3/` and run `pip install -r requirements.txt`.
* Change into `task-4/` and run `pip install -r requirements.txt`.
* To run task 4 using docker-compose, change into `task-4/` and run `docker-compose up`.
